import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Grid} from './grid.js';

ReactDOM.render(<Grid />, document.getElementById('root'));