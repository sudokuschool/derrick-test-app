import React from 'react';
import ReactDOM from 'react-dom';
import {Cell} from './cell.js'

export class Grid extends React.Component {
	render() {
	var values = '016002400320009000040103000005000069009050300630000800000306010000400072004900680'
	var grid = []
	for (let i=0; i<9; i++){
		var tmp = []
		for (let j=0; j<9; j++){
			tmp.push(values[(i*9)+j])
		}
		grid.push(tmp)
	}																						
	return (
			<table>
				{
					grid.map(function(row, x){ 
					return( <tr>
						{row.map(function(elem, y){
						return (<td key={x+', '+y}> <Cell val={grid[x][y]} /> </td>) })}
					</tr>)})
				}
			</table>
			)
	}
}